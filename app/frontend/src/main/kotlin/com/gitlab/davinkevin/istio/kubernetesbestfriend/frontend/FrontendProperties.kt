package com.gitlab.davinkevin.istio.kubernetesbestfriend.frontend

import org.springframework.boot.context.properties.ConfigurationProperties
import java.net.URI

@ConfigurationProperties("frontend")
class FrontendProperties(
        val name: String = "frontend",
        val version: String = "v0",
        val maxLatency: Int = 0,
        val errorRate: Int = 0,
        val middleware: MiddlewareProperties
) {
        data class MiddlewareProperties(val url: URI)
}
