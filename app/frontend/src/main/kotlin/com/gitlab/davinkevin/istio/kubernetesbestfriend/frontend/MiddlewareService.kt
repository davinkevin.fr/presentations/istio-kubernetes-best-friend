package com.gitlab.davinkevin.istio.kubernetesbestfriend.frontend

import org.slf4j.LoggerFactory
import org.springframework.web.client.RestClient
import org.springframework.web.client.body
import java.time.ZonedDateTime

class MiddlewareService(
    rcb: RestClient.Builder,
    private val prop: FrontendProperties
) {

    private val log = LoggerFactory.getLogger(MiddlewareService::class.java)
    private val rc = rcb.baseUrl(prop.middleware.url.toASCIIString()).build()

    fun call(): Message? {

        log.info("Before call to MiddlewareService at url ${prop.middleware.url}")

        val message = rc.get().retrieve().body<Message>()

        log.info("Call made to ${prop.middleware.url}")

        return message
    }
}

data class Message(
    val from: String,
    val date: ZonedDateTime = ZonedDateTime.now()
)
