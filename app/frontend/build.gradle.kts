import org.gradle.internal.deprecation.DeprecatableConfiguration

plugins {
	id("org.springframework.boot") version "3.4.3"
	id("io.spring.dependency-management") version "1.1.7"
	kotlin("jvm") version "2.1.10"
	kotlin("plugin.spring") version "2.1.10"
	id("com.google.cloud.tools.jib") version "3.4.4"
}

group = "com.gitlab.davinkevin.istio.kubernetes-best-friend"
version = "0.0.1-SNAPSHOT"

java {
	toolchain {
		languageVersion = JavaLanguageVersion.of(21)
	}
}

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("io.micrometer:micrometer-tracing-bridge-brave")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
	testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

kotlin {
	compilerOptions {
		freeCompilerArgs.addAll("-Xjsr305=strict")
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

jib {
	from {
		image = "gcr.io/distroless/java21-debian12"
	}
	to {
		if (System.getenv("CI")?.toBooleanStrictOrNull() == true) {
			val registry = System.getenv("CI_REGISTRY")
			val projectPath = System.getenv("CI_PROJECT_PATH")
			val sha = System.getenv("CI_COMMIT_SHA")
			image = "$registry/$projectPath/${project.name}:$sha"
		} else {
			image = "registry.gitlab.com/davinkevin.fr/presentations/istio-kubernetes-best-friend/frontend:latest"
		}
	}
}

tasks.register("downloadDependencies") {
	fun Configuration.isDeprecated(): Boolean = when (this) {
		is DeprecatableConfiguration -> resolutionAlternatives.isNotEmpty()
		else -> false
	}
	doLast {
		val buildDeps = buildscript
			.configurations
			.onEach { it.incoming.artifactView { lenient(true) }.artifacts }
			.sumOf { it.resolve().size }

		val allDeps = configurations
			.filter { it.isCanBeResolved && !it.isDeprecated() }
			.onEach { it.incoming.artifactView { lenient(true) }.artifacts }
			.map { runCatching { it.resolve() }.getOrElse { emptySet() } }
			.sumOf { it.size }

		println("Downloaded all dependencies: ${allDeps + buildDeps}")
	}
}
