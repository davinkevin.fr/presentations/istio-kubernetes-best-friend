package com.gitlab.davinkevin.istio.kubernetesbestfriend.database

import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.router
import java.time.ZonedDateTime
import java.time.ZonedDateTime.now
import java.util.concurrent.TimeUnit

@Configuration
@EnableConfigurationProperties(DatabaseProperties::class)
@Import(DatabaseHandler::class)
class ApplicationConfiguration {
    @Bean
    fun routes(database: DatabaseHandler) = router {
        GET("/", database::serve)
    }
}

class DatabaseHandler(
        prop: DatabaseProperties
) {

    private val log = LoggerFactory.getLogger(DatabaseHandler::class.java)

    private val name = prop.name
    private val version: String = prop.version
    private val requestWaitingRange = (0..prop.maxLatency).map(Int::toLong)

    private val errorProbability = 0..100
    private val errorRate = prop.errorRate

    fun serve(@Suppress("UNUSED_PARAMETER") r: ServerRequest): ServerResponse {

        val error = errorProbability.shuffled().first() < errorRate

        if (error) {
            log.error("Random error 🔥")
            throw ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Random error 🔥")
        }

        val duration = requestWaitingRange.shuffled().first()
        TimeUnit.MILLISECONDS.sleep(duration)

        log.info("$name called and it responded with \"$name: $version\" after $duration ms")

        return ServerResponse.ok().body(Message(from = "$name ($version)", date = now()))
    }
}

data class Message(
        val from: String,
        val date: ZonedDateTime = now()
)
