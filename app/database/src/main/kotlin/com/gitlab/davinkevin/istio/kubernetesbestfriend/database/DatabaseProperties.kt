package com.gitlab.davinkevin.istio.kubernetesbestfriend.database

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("database")
class DatabaseProperties(
        val name: String = "database",
        val version: String = "v0",
        val maxLatency: Int = 0,
        val errorRate: Int = 0
)
