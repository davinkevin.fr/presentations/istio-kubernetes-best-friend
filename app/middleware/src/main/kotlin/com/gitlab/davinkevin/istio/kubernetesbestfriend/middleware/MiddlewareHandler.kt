package com.gitlab.davinkevin.istio.kubernetesbestfriend.middleware

import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.router
import java.time.ZonedDateTime.now
import java.util.concurrent.TimeUnit

@Configuration
@EnableConfigurationProperties(MiddlewareProperties::class)
@Import(MiddlewareHandler::class, DatabaseService::class)
class ApplicationConfiguration {
    @Bean
    fun routes(middleware: MiddlewareHandler) = router {
        GET("/", middleware::serve)
    }
}

class MiddlewareHandler(
    private val database: DatabaseService,
    prop: MiddlewareProperties
) {

    private val log = LoggerFactory.getLogger(MiddlewareHandler::class.java)

    private val name = prop.name
    private val version: String = prop.version
    private val requestWaitingRange = (0..prop.maxLatency).map(Int::toLong)

    private val errorProbability = 0..100
    private val errorRate = prop.errorRate

    @SuppressWarnings("")
    fun serve(@Suppress("UNUSED_PARAMETER") r: ServerRequest): ServerResponse {

        val duration = requestWaitingRange.shuffled().first()

        val error = errorProbability.shuffled().first() < errorRate

        if (error) {
            log.error("Random error 🔥")
            throw ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Random error 🔥")
        }

        log.info("UI Service in version $version starting...")

        TimeUnit.MILLISECONDS.sleep(duration)

        val message = database.call()

        if (message == null) {
            log.error("error returned from database 🔥")
            throw ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "error returned from database 🔥")
        }

        TimeUnit.MILLISECONDS.sleep(duration)

        val newMessage = message.copy(from = "$name ($version) => ${message.from}", date = now())

        log.info("$name service in version $version called and answered with $newMessage")

        return ServerResponse.ok().body(newMessage)
    }
}
