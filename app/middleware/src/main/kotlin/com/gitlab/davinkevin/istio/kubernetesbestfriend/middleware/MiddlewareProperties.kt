package com.gitlab.davinkevin.istio.kubernetesbestfriend.middleware

import org.springframework.boot.context.properties.ConfigurationProperties
import java.net.URI

@ConfigurationProperties("middleware")
class MiddlewareProperties(
        val name: String = "middleware",
        val version: String = "v0",
        val maxLatency: Int = 0,
        val errorRate: Int = 0,
        val database: DatabaseProperties
) {
        data class DatabaseProperties(val url: URI)
}
