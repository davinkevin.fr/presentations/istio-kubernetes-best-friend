package com.gitlab.davinkevin.istio.kubernetesbestfriend.middleware

import org.slf4j.LoggerFactory
import org.springframework.web.client.RestClient
import org.springframework.web.client.body
import java.time.ZonedDateTime

class DatabaseService(
    rcb: RestClient.Builder,
    private val prop: MiddlewareProperties
) {

    private val log = LoggerFactory.getLogger(DatabaseService::class.java)
    private val rc = rcb.baseUrl(prop.database.url.toASCIIString()).build()

    fun call(): Message? {

        log.info("Before call to DatabaseService at url ${prop.database.url}")

        val message = rc.get().retrieve().body<Message>()

        log.info("Call made to ${prop.database.url}")

        return message
    }
}

data class Message(
    val from: String,
    val date: ZonedDateTime = ZonedDateTime.now()
)
