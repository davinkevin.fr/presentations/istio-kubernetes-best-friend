
interface Cache {
    fun findById(id: String): String?
}
interface Database {
    fun findById(id: String): String?
}

class Service(val cache: Cache, val db: Database) {
    fun foo() = 2
    fun bar() = 3

    fun fetch(id: String): String? {
        val dataFromCache = cache.findById(id)
        if (dataFromCache != null) {
            return dataFromCache
        }

        return db.findById(id)
    }
}
class Async {
    fun await() = 1
}
fun async(v : () -> Any) = Async()

fun fetch(svc: Service): Int {
    val foo = async { svc.foo() }.await()
    val bar = async { svc.bar() }.await()

    return foo + bar
}

