#!/usr/bin/env bash

export KUBECONFIG=$(mktemp)
k3d kubeconfig get istio-kubernetes-best-friend > "$KUBECONFIG"
source .task/includes/demo/.demo-magic.sh

DEMO_PROMPT="\e[92;5;69m❯ \[\e[0m\]"
NO_WAIT=true

function reset_ns() {
  kubectl apply -k k8s/app/overlays/gateways-way/02-with-istio > /dev/null
}

function wac() {
  wait && clear
}

function next_part() {
    wait && clear && wait
}

reset_ns
clear && wait

# Mirroring
p "kubectl apply -k mirroring"
kustomize build k8s/app/overlays/gateways-way/03-mirroring | kubectl apply -f - > /dev/null
echo "service/middleware-v1 created"
echo "service/middleware-v2 created"
echo "httproute.gateway.networking.k8s.io/middleware configured"
p "cat mirroring/middleware.yaml"
bat k8s/app/overlays/gateways-way/02-with-istio/network.yaml --style "numbers,grid" --line-range 30:40
wac
bat k8s/app/overlays/gateways-way/02-with-istio/network.yaml --style "numbers,grid" --line-range 42:52
wac
bat k8s/app/overlays/gateways-way/03-mirroring/middleware.yaml --style "numbers,grid" --line-range 1:10
wac
bat k8s/app/overlays/gateways-way/03-mirroring/middleware.yaml --style "numbers,grid" --line-range 11:20
next_part

# Canary
p "kubectl apply -k canary"
kubectl apply -k k8s/app/overlays/gateways-way/04-canary > /dev/null
echo "httproute.gateway.networking.k8s.io/middleware configured"
wac
p "cat canary/middleware.yaml"
bat k8s/app/overlays/gateways-way/04-canary/middleware.yaml --style "numbers,grid" --line-range 11:21
next_part

# Traffic Splitting
kubectl apply -k k8s/app/overlays/gateways-way/05-traffic-splitting/01-90-10 > /dev/null
p "kubectl apply -k traffic-splitting/90-10"
echo "httproute.gateway.networking.k8s.io/middleware configured"
p "cat traffic-splitting/90-10/middleware.yaml"
bat k8s/app/overlays/gateways-way/05-traffic-splitting/01-90-10/middleware.yaml --style "numbers,grid" --line-range 11:18
next_part

kubectl apply -k k8s/app/overlays/gateways-way/05-traffic-splitting/02-50-50 > /dev/null
p "kubectl apply -k traffic-splitting/50-50"
echo "httproute.gateway.networking.k8s.io/middleware configured"
p "cat manifests/middleware.yaml"
bat k8s/app/overlays/gateways-way/05-traffic-splitting/02-50-50/middleware.yaml --style "numbers,grid" --line-range 11:18
next_part

kubectl apply -k k8s/app/overlays/gateways-way/05-traffic-splitting/03-0-100 > /dev/null
p "kubectl apply -k traffic-splitting/0-100"
echo "httproute.gateway.networking.k8s.io/middleware configured"
p "cat manifests/middleware.yaml"
bat k8s/app/overlays/gateways-way/05-traffic-splitting/03-0-100/middleware.yaml --style "numbers,grid" --line-range 11:18
next_part