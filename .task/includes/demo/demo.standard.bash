#!/usr/bin/env bash

export KUBECONFIG=$(mktemp)
k3d kubeconfig get istio-kubernetes-best-friend > "$KUBECONFIG"
source .task/includes/demo/.demo-magic.sh

DEMO_PROMPT="\e[92;5;69m❯ \[\e[0m\]"
NO_WAIT=true

function reset_ns() {
  kustomize build k8s/app/overlays/istios-way/02-with-istio | kubectl apply -f - > /dev/null
}

reset_ns
clear
wait

# Apply mirroring
kustomize build k8s/app/overlays/istios-way/03-mirroring | kubectl apply -f - > /dev/null
p "cat mirroring/middleware.yaml"
bat k8s/app/overlays/istios-way/03-mirroring/middleware.yaml --style "numbers,grid" --line-range 0:15
wait
clear
bat k8s/app/overlays/istios-way/03-mirroring/middleware.yaml --style "numbers,grid" --line-range 18:99
wait
clear

p "kubectl apply -k mirroring"
echo "virtualservice.networking.istio.io/middleware created"
echo "destinationrule.networking.istio.io/middleware created"
wait
clear

# Canary
p "cat canary/middleware.yaml"
bat k8s/app/overlays/istios-way/04-canary/middleware.yaml --style "numbers,grid" --line-range 9:21
wait
p "kubectl apply -k canary"
kubectl apply -k k8s/app/overlays/istios-way/04-canary > /dev/null
echo "virtualservice.networking.istio.io/middleware configured"
wait
clear

# Traffic Splitting
kubectl apply -k k8s/app/overlays/istios-way/05-traffic-splitting/01-90-10 > /dev/null

p "kubectl apply -k traffic-splitting/90-10"
echo "virtualservice.networking.istio.io/middleware configured"
p "cat traffic-splitting/90-10/middleware.yaml"
bat k8s/app/overlays/istios-way/05-traffic-splitting/01-90-10/middleware.yaml --style "numbers,grid" --line-range 9:18
wait
clear

kubectl apply -k k8s/app/overlays/istios-way/05-traffic-splitting/02-50-50 > /dev/null
p "kubectl apply -k traffic-splitting/50-50"
echo "virtualservice.networking.istio.io/middleware configured"
p "cat manifests/middleware.yaml"
bat k8s/app/overlays/istios-way/05-traffic-splitting/02-50-50/middleware.yaml --style "numbers,grid" --line-range 9:18
wait
clear

kubectl apply -k k8s/app/overlays/istios-way/05-traffic-splitting/03-0-100 > /dev/null
p "kubectl apply -k traffic-splitting/0-100"
echo "virtualservice.networking.istio.io/middleware configured"
p "cat manifests/middleware.yaml"
bat k8s/app/overlays/istios-way/05-traffic-splitting/03-0-100/middleware.yaml --style "numbers,grid" --line-range 9:18
wait
clear